from django.shortcuts import render, redirect
from django.views.generic import View, CreateView, ListView
from django.contrib.auth import login, logout
from django.contrib.auth.views import LoginView
from .forms import RegisterUserForm, LoginUserForm, NoteForm
from .models import Note


class RegisterUser(CreateView):
    form_class = RegisterUserForm
    template_name = 'register.html'

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('notes')


class LoginUser(LoginView):
    form_class = LoginUserForm
    template_name = 'login.html'


class LogoutUser(View):
    def get(self, *args, **kwargs):
        logout(self.request)
        return redirect('home')


class Home(View):
    template_name = 'index.html'

    def get(self, request):
        count = Note.objects.filter(user_id=self.request.user.id).count()
        return render(request, self.template_name, {'count': count})


class Notes(ListView):
    model = Note
    template_name = 'notes.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['count'] = Note.objects.filter(user_id=self.request.user.id).count()
        return context

    def get_queryset(self):
        query_set = Note.objects.filter(user_id=self.request.user.id)
        return query_set


class NotesAdd(View):
    model = Note
    template_name = 'notes_add.html'

    def get(self, request):
        note_form = NoteForm()
        return render(request, self.template_name, {'form': note_form})

    def post(self, request):
        note_form = NoteForm(request.POST)
        note_add = note_form.save(commit=False)
        note_add.user_id = request.user.id
        note_add.save()
        return redirect('notes')
