from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from .models import User, Note


class RegisterUserForm(UserCreationForm):

    class Meta:
        model = User
        fields = ['email', 'password1', 'password2']

    email = forms.EmailField(
        error_messages={
            'unique': 'Пользователь с таким e-mail уже существует',
            'required': 'Введите e-mail',
        },
        label='E-Mail',
        widget=forms.EmailInput(attrs={'autofocus': True, 'class': 'frm-ctl-lg'})
    )
    password1 = forms.CharField(
        error_messages={'required': 'Введите пароль'},
        label='Пароль',
        widget=forms.PasswordInput(attrs={'class': 'frm-ctl-lg'})
    )
    password2 = forms.CharField(
        error_messages={'required': 'Введите пароль'},
        label='Повтор пароля',
        widget=forms.PasswordInput(attrs={'class': 'frm-ctl-lg'})
    )


class LoginUserForm(AuthenticationForm):
    username = forms.EmailField(
        error_messages={'required': 'Введите e-mail'},
        label='E-Mail',
        widget=forms.EmailInput(attrs={'autofocus': True, 'class': 'frm-ctl-lg'})
    )
    password = forms.CharField(
        error_messages={'required': 'Введите пароль'},
        label='Пароль',
        widget=forms.PasswordInput(attrs={'class': 'frm-ctl-lg'})
    )


class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = ['remark']

    remark = forms.CharField(
        label='Заметка',
        widget=forms.Textarea(attrs={'autofocus': True, 'class': 'frm-ctl-d'})
    )
