from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class UserManager(BaseUserManager):

    def create_user(self, email, password, **kwargs):
        user = self.model(email=self.normalize_email(email), **kwargs)
        user.set_password(password)
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):

    USERNAME_FIELD = 'email'
    email = models.EmailField(verbose_name='E-Mail', unique=True)
    objects = UserManager()

    def __str__(self):
        return self.email


class Note(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    remark = models.TextField(verbose_name='Заметка')
    date_saved = models.DateTimeField(default=timezone.now, verbose_name='Время создания')
    objects = models.Manager()

    def __str__(self):
        return self.remark
