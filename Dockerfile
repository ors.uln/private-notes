FROM python:3.10-alpine
ENV PYTHONUNBUFFERED=1
WORKDIR /code
RUN apk add --no-cache gcc musl-dev linux-headers libpq-dev
COPY ./requirements.txt /code/
RUN pip install -r requirements.txt
